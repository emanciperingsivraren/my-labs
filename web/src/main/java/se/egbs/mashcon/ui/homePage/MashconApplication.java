package se.egbs.mashcon.ui.homePage;

import de.agilecoders.wicket.core.Bootstrap;
import de.agilecoders.wicket.core.settings.BootstrapSettings;
import org.apache.wicket.Application;
import org.apache.wicket.Page;
import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.spring.injection.annot.SpringComponentInjector;


/**
 * Created by Per-Olof on 2015-03-12.
 */
public class MashconApplication extends WebApplication {


    @Override
    public Class<? extends Page> getHomePage() {
        return HomePage.class;
    }

    @Override
    protected void init() {
        super.init();

        //LOGGER.info("Initializing...");
        BootstrapSettings bootstrapSettings = new BootstrapSettings();
        Bootstrap.install(Application.get(), bootstrapSettings);

        SpringComponentInjector componentInjector = new SpringComponentInjector(this);
        getComponentInstantiationListeners().add(componentInjector);


        mapPages();

        System.out.println("We habe started");

    }


    private void mapPages() {
        mountPage("/home", HomePage.class);

    }





}
