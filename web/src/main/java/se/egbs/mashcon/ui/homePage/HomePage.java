package se.egbs.mashcon.ui.homePage;

import de.agilecoders.wicket.core.markup.html.bootstrap.html.HtmlTag;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;

/**
 * Created by Per-Olof on 2015-03-11.
 */
public class HomePage extends WebPage {


    public HomePage() {
        super();

        add(new Label("message", "Wicked message"));    }
}
